﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjetTest
{
	public class List
	{
		public static string Concatenate(List<string> aTestList)
		{
			return string.Join(",", aTestList.ToArray());
		}

		public static List<string> ReverseList(List<string> list)
		{
			return list.Reverse<string>().ToList();
		}

		public static List<string> ToUppercase(List<string> aTestList)
		{
			return aTestList.Select(x => x.ToUpper()).ToList();
		}
	}
}
