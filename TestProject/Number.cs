﻿using System.Collections.Generic;
using System.Linq;

namespace ProjetTest
{
	public class Number
	{
		/// <summary>
		/// numbers
		///		key : the number
		///		value : the number of occurrences in the table
		///</summary>
		public static IDictionary<int, int> OrdererCount(int[] numbers)
		{
			var occurrences = new Dictionary<int, int>();
			
            foreach (int n in numbers)
			{
				if (occurrences.ContainsKey(n)) // Check if exists
					occurrences[n]++; 
				else
					occurrences.Add(n, 1); //If it does not exists, add it to the Dictionary
			}
			return occurrences.OrderBy(x => x.Key).ToDictionary(p => p.Key, p => p.Value); 
		}

	}
}
